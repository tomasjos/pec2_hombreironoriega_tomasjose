PEC2_HombreiroNoriega_TomásJose
El proyecto de juego de plataformas es un ejemplo de como emplear las herramientas de unity 2d para crear un nivel de juego imitando el conocido juego supermario.

Para crear el juego se ha aprovechado la plantilla de la PEC2 entregada por el profesor.

Se ha añadido una serie de sprites que se han obtenido de la página indicada por el profesor, y que se han empleado para crear el suelo y las plataformas, así como los 
bloques de piedra, los de interrogación y los vacios, así como el personaje  del que se han usado diferentes sprites para el salto, la carrera o la eliminación, 
y también sprites de edificio, cajas, arbustos, setas -los mushrooms, villanos del juego- y supersetas -setas naranjas- 
Se han creado asimismo dos sprites, uno para monedas y otro para puntos. 

En cuanto al código del juego, se han creado una serie de scripts que realizan las tareas indicadas por el enunciado.

movimientopersona.cs es el script que crea el objeto personaje, implementa los movimientos hacia adelante -tecla cursor derecha-, hacia atras -tecla 
cursor izquierda y salto -tecla cursor hacia arriba-, así como las funciones de colision
en las que se define la acción que se produce cuando el personaje impacta con un bloque interrogación, un bloque de piedra, una seta,  un deadzone 
o el fin del nivel. Se ha usado una funcion random para generar aleatoriamente el premio que sale al impactar con los bloques -monedas, powerups y supermushroom-. Posteriormente se crea el objeto correspondiente en 
el escenario, y se programa una duración de varios segundos de visibilidad para los premios. En el caso del supermushroom la duración de la energia extra es de 
diez segundo.

bikefollower.cs es el script original en el que se acoplaba la camara al personaje para que le siga.

deadzone.cs, seta.cs, edificio.cs son scripts que inician los respectivos objetos y generan las funciones de colision correspondientes.

movimientobolsa.cs es el script asociado a la moneda/powerup/supermushroom, que implementa la función colision.

gameplaymanager.cs gestiona el inicio y fin del nivel , su reinicio así como la información sobre el juego que debe aparecer -tiempo, puntos, monedas, situacion
del juego-.

Se han creado materiales nuevos -friccion-para aumentar el rozamiento y se ha cambiado el centro de gravedad del personaje.
