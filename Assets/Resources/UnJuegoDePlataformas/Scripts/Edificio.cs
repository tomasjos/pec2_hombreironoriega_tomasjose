﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Edificio : MonoBehaviour
{
    // Start is called before the first frame update
    string result = "";
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public GameplayManager GameplayManager;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if ((collision.gameObject.CompareTag("Persona")))
        {
            result = "Ganaste!!";
            Debug.Log("Ganaste la partida");
            GameplayManager.Result.text = "Ganaste!!";
            GameplayManager.EndGame();
            Invoke("reiniciarnivel", 2f);

        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((collision.CompareTag("Persona")))
        {

            GameplayManager.RestartLevel();
        }
    }
    private void reiniciarnivel()
    {
        GameplayManager.RestartLevel();
    }
}
