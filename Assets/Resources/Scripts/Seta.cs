﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seta : MonoBehaviour
{
    string result = "";
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public GameplayManager GameplayManager;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (((collision.gameObject.CompareTag("Persona"))&&(collision.gameObject.GetComponent<MovimientoPersona>().super==false)))
        {
            result= "Game Over Perdiste";
            Debug.Log("Perdiste, chocaste con una seta");
            Invoke("reiniciarnivel", 2f);
           
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((collision.CompareTag("Bike")) || (collision.CompareTag("Persona")))
        {
            GameplayManager.RestartLevel();
        }
    }
    private void reiniciarnivel()
        {
        GameplayManager.RestartLevel();
    }
}
