﻿using System;
using System.Collections;            
using System.Collections.Generic;
using UnityEngine;

public class MovimientoPersona : MonoBehaviour
{
    public float LinearSpeed = 10;
    public float JumpLevel = 300;
    public int puntos=0;
    public int monedas = 0;
    public bool super = false;
   // public Transform bloquevacio;
   [SerializeField]
    public GameObject moneda;
    [SerializeField]
    public GameObject auxiliar;
    public Action OnKilled;
    public Action OnReachedEndOfLevel;
    public bool saltar=true;
    public bool perder = false;
    public bool ganar = false;
    public Transform suelo;
    public float distanciatierra = 0.5f;
    public LayerMask estierra;
    private Rigidbody2D rb;
    private Sprite avatar;
    private int contactos = 0;
    private float Radius;    // Start is called before the first frame update
    private void Start()
    {
        GetComponent<Rigidbody2D>().centerOfMass = new Vector2((float)0.0, (float)1.5);
    }
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        suelo = GameObject.Find("Suelo").transform;
    }
    private void FixedUpdate()
    {
     
       
    }
    // Update is called once per frame
    void Update()
    {
        saltar = Physics2D.OverlapCircle(suelo.position, distanciatierra, estierra);
        if (perder==false)
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                MoveBackward();
               /* if (Input.GetKey(KeyCode.UpArrow))
                {
                    GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/SuperMario/Jump");
                   
                    if (saltar == true)
                    {
                        RotateRight();
                        MoveBackward();
                    }
                    else
                    {

                    }
                }
                else
                {
                    GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/SuperMario/Run");
                    MoveBackward();
                }*/
            }
            if (Input. GetKey(KeyCode.RightArrow))
            {
                MoveForward();
               /* if (Input.GetKey(KeyCode.UpArrow))
                {
                    GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/SuperMario/Jump");
                    
                    if (saltar==true)
                    {
                        RotateLeft();
                        MoveForward();
                    }
                   // MoveForward();
                }
                else
                {
                    GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/SuperMario/Run");
                    MoveForward();
                }*/

            }
            if ((saltar == true) && (Input.GetKeyDown(KeyCode.UpArrow)))
            {
                Jump();
            }
            if ((!Input.GetKey(KeyCode.RightArrow) &&(!Input.GetKey(KeyCode.LeftArrow))&&(!Input.GetKey(KeyCode.UpArrow))))
            {
                GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/SuperMario/Persona");
            }
           // GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("UnJuegoDePlataformas/Sprites/SuperMario/Persona");
        }
        else
        {

        }
        
       /* else
        {
           Stop();
        }
        */
    }
    private void MoveForward()
    {
        if (IsGrounded())
        {
            
            var right = transform.right;
            rb.velocity += new Vector2(right.x * LinearSpeed, right.y ) * Time.deltaTime;
       }
    }
    private void RotateLeft()
    {
        if (IsGrounded())
        {
            var right = transform.right;
            rb.SetRotation(-10);

        }
    }
    private void RotateRight()
    {
        if (IsGrounded())
        {
            var right = transform.right;
            rb.SetRotation(10);

        }
    }
    private void MoveBackward()
    {
         if (IsGrounded())
          {
        GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/SuperMario/Run");
        var right = transform.right;
            rb.velocity -= new Vector2(right.x * LinearSpeed, right.y ) * Time.deltaTime;
        }
    }
    private void Jump()
    {
        GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/SuperMario/Jump");
        var right = transform.right;
        // transform.Rotate(new Vector2(0, 1) * Time.deltaTime * LinearSpeed, Space.World);
       
                    rb.AddForce (new Vector2(0, JumpLevel));
            
       
           
    }
    private void Down()
    {
        if (!IsGrounded())
        {
            var right = transform.up;
         //   transform.Rotate(new Vector2(0, -1) * Time.deltaTime * LinearSpeed, Space.World);
            rb.velocity = new Vector2(0, right.y * LinearSpeed) * Time.deltaTime;
        }
    }
    private void Stop()
    {
        if (IsGrounded())
        {
            var right = transform.right;
            rb.velocity += new Vector2(0, 0) * Time.deltaTime;
        }
    }
    private void OnCollisionEnter2D(Collision2D coll)
    {
        //Debug.Log("Physics collision!");
       
        if (coll.gameObject.CompareTag("bloqueinterrogacion"))
        {
           
           // Destroy(coll.gameObject);
            String nombresprite = "Sprites/TerrainSuperMario/bloquevacio";
            coll.gameObject.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(nombresprite);
            coll.gameObject.GetComponent<Transform>().tag = "bloquevacio";
            Debug.Log(moneda);
            System.Random random = new System.Random();
           
            int inicial = random.Next(0, 3);
            Debug.Log(inicial);
            if (inicial==0)
            {
                Debug.Log("choque con bloque, gano una moneda");
                monedas = monedas + 1;
                Debug.Log("moneda existe");
                auxiliar = Instantiate(moneda);
                nombresprite = "Sprites/TerrainSuperMario/moneda";
                Sprite sprites = Resources.Load<Sprite>(nombresprite);
                auxiliar.transform.tag = "moneda";
                auxiliar.transform.position = new Vector3(coll.transform.position.x, coll.transform.position.y);
                auxiliar.SetActive(true);
                Rigidbody2D rbbloquevacio = auxiliar.AddComponent<Rigidbody2D>() as Rigidbody2D;
                BoxCollider2D clbloquevacio = auxiliar.AddComponent<BoxCollider2D>() as BoxCollider2D;
                SpriteRenderer srbloquevacio = auxiliar.AddComponent<SpriteRenderer>() as SpriteRenderer;
                srbloquevacio.sprite = sprites;
                Debug.Log(sprites);
                auxiliar.GetComponent<Rigidbody2D>().position = auxiliar.transform.position;
                auxiliar.GetComponent<Rigidbody2D>().mass = 1;
                auxiliar.GetComponent<Rigidbody2D>().gravityScale = 0;
                auxiliar.GetComponent<BoxCollider2D>().isTrigger = false;
                //  moneda.GetComponent<BoxCollider2D>().offset.Set(0, 0);
                //  moneda.GetComponent<BoxCollider2D>().size.Set((float)0.08, (float)0.48);
                auxiliar.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 300));
                MovimientoBolsa movbol = auxiliar.AddComponent<MovimientoBolsa>() as MovimientoBolsa;

                auxiliar.layer = 8;
                auxiliar.GetComponent<SpriteRenderer>().sortingOrder = 1;
               // Invoke("Destruirmoneda", 2f);
                // Destroy(moneda);
            }
            else if (inicial==1)
            {
                Debug.Log("choque con bloque, gano 100 puntos");
                puntos = puntos + 100;
                auxiliar = Instantiate(moneda);
                nombresprite = "Sprites/TerrainSuperMario/cienpuntos";
                Sprite sprites = Resources.Load<Sprite>(nombresprite);
                auxiliar.transform.tag = "powerup";
                auxiliar.transform.position = new Vector3(coll.transform.position.x, coll.transform.position.y);
                auxiliar.SetActive(true);
                Rigidbody2D rbbloquevacio = auxiliar.AddComponent<Rigidbody2D>() as Rigidbody2D;
               // BoxCollider2D clbloquevacio = auxiliar.AddComponent<BoxCollider2D>() as BoxCollider2D;
                SpriteRenderer srbloquevacio = auxiliar.AddComponent<SpriteRenderer>() as SpriteRenderer;
                srbloquevacio.sprite = sprites;
                Debug.Log(sprites);
                
                auxiliar.GetComponent<Rigidbody2D>().position = auxiliar.transform.position;
                auxiliar.GetComponent<Rigidbody2D>().mass = 1;
                auxiliar.GetComponent<Rigidbody2D>().gravityScale = 0;
               // auxiliar.GetComponent<BoxCollider2D>().isTrigger = false;
                //  moneda.GetComponent<BoxCollider2D>().offset.Set(0, 0);
                //  moneda.GetComponent<BoxCollider2D>().size.Set((float)0.08, (float)0.48);
                auxiliar.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 100));
                MovimientoBolsa movbol = auxiliar.AddComponent<MovimientoBolsa>() as MovimientoBolsa;

                auxiliar.layer = 8;
                auxiliar.GetComponent<SpriteRenderer>().sortingOrder = 1;

               // Invoke("Destruirmoneda", 2f);
            }
            else if (inicial == 2)
            {
                Debug.Log("choque con bloque, basa a nivel Super");
                super = true;
                Invoke("nivelnormal", 10f);
                auxiliar = Instantiate(moneda);
                nombresprite = "Sprites/TerrainSuperMario/superseta";
                Sprite sprites = Resources.Load<Sprite>(nombresprite);
                auxiliar.transform.tag = "superseta";
                auxiliar.transform.position = new Vector3(coll.transform.position.x, coll.transform.position.y);
                auxiliar.SetActive(true);
                gameObject.transform.localScale.Set((float)0.30, (float)0.30,(float) 0.30);
                gameObject.GetComponent<Transform>().localScale = new Vector3((float)0.30,(float)0.30,(float)0.30);
                Rigidbody2D rbbloquevacio = auxiliar.AddComponent<Rigidbody2D>() as Rigidbody2D;
               // BoxCollider2D clbloquevacio = auxiliar.AddComponent<BoxCollider2D>() as BoxCollider2D;
                SpriteRenderer srbloquevacio = auxiliar.AddComponent<SpriteRenderer>() as SpriteRenderer;
                srbloquevacio.sprite = sprites;
                Debug.Log(sprites);

                auxiliar.GetComponent<Rigidbody2D>().position = auxiliar.transform.position;
               // auxiliar.GetComponent<BoxCollider2D>().isTrigger = false;
                auxiliar.GetComponent<Rigidbody2D>().mass = 1;
                auxiliar.GetComponent<Rigidbody2D>().gravityScale = 0;
                //  moneda.GetComponent<BoxCollider2D>().offset.Set(0, 0);
                //  moneda.GetComponent<BoxCollider2D>().size.Set((float)0.08, (float)0.48);
                auxiliar.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 100));
                MovimientoBolsa movbol = auxiliar.AddComponent<MovimientoBolsa>() as MovimientoBolsa;

                auxiliar.layer = 8;
                auxiliar.GetComponent<SpriteRenderer>().sortingOrder = 1;

                // Invoke("Destruirmoneda", 2f);
            }


        }
      
        else if((coll.gameObject.CompareTag("seta"))&&(super==false))
        {
            Debug.Log("Perdiste");
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/SuperMario/Dead");
            transform.Rotate(new Vector3(0,0,1),-90);
            perder = true;
            
        }
        else if(coll.gameObject.CompareTag("GameOver"))
        {
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/SuperMario/Dead");
            Debug.Log("Perdiste");
            perder = true;
        }
        else if(coll.gameObject.CompareTag("EndOfLevel"))
        {
            Debug.Log("ganaste");
            ganar = true;
            GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/SuperMario/Persona");

        }
        
    }

    private void Destruirmoneda()
    {
        Debug.Log("objeto moneda/puntos/super destruido");
        Destroy(auxiliar);
    }
    private void nivelnormal()
    {
        Debug.Log("vuelve a nivel normal");
        super = false;
        gameObject.GetComponent<Transform>().localScale = new Vector3((float)0.187588, (float)0.187588, (float)0.187588);

    }
    private bool IsGrounded()
    {
        // return Physics2D.OverlapCircleAll(suelo.position,(float)0.1).Length >= 1;
        return Physics2D.OverlapCircle(suelo.position, distanciatierra, estierra);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
      //  Debug.Log("Trigger collision!");

        if (other.gameObject.CompareTag("EndOfLevel"))
        {
            OnReachedEndOfLevel?.Invoke();

        }
        else if ((!other.gameObject.CompareTag("Suelo"))&&(!other.gameObject.CompareTag("bloquepiedra")))
        {
            saltar = false;
        }
        else
        {
            OnKilled?.Invoke();
            
        }
    }
}


